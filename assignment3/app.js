const app = Vue.createApp({
    data() {
        return {
            counter: 0,
            message: ''
        };
    },
    methods: {
        addFive(){
            this.counter += 5;
        },
        addOne(){
            this.counter++;
        }
    },
    watch: {
        counter(value) {
            if(value < 37)
                this.message = 'Not there yet';
            else if(value > 37)
                this.message = 'Too much!';
            else
                this.message = this.counter;
            console.log(this.counter);
        },
        message(){
            const that = this;
            setTimeout(function() {
                that.counter = 0;
            }, 5000);
        }
    }
});

app.mount("#assignment");