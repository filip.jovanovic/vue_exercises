const app = Vue.createApp({
    data() {
        return {
           output: '',

           secondOutput: '',
           confirmedOutput: ''
        };
    },
    methods: {
        showAlert(){
            alert('Alert message');
        },
        updateOutput(event){
            this.output = event.target.value;
        },

        confirmOutput(e){
            this.confirmedOutput = this.secondOutput;
        },
        updateSecondOutput(event){
            this.secondOutput = event.target.value;
        }
    }
});

app.mount("#assignment");