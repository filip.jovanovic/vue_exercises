const app = Vue.createApp({
    data() {
        return {
            tasks: [],
            enteredTask: '',

            buttonCaption: 'Hide',
            visibility: true
        };
    },
    methods: {
        addTask(){
            this.tasks.push(this.enteredTask);
            this.enteredTask = '';
        },
        changeVisibility(){
            this.buttonCaption = this.buttonCaption === 'Hide' ? 'Show' : 'Hide';
            this.visibility = !this.visibility;
        }
    }
});

app.mount('#assignment');
