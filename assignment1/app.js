const app = Vue.createApp({
    data() {
        return {
            myName: 'Filip',
            myAge: 22,
            image: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRuXFIW3zFpChPpIy5dtCtK03FOKl0cZueBJw&usqp=CAU"
        };
    },
    methods: {
        randomNumber() {
            return Math.random();
        } 
    }
});

app.mount("#assignment");