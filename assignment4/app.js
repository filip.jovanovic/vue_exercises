const app = Vue.createApp({
    data(){
        return {
            classInput: '',
            visibility: 'visible',
            bgColor: ''
        };
    },
    methods:{
        changeClass(event){
            this.classInput = event.target.value;
        },
        changeVisibility(){
            this.visibility = this.visibility === 'visible' ? 'hidden' : 'visible';
        },
        changeBgColor(event){
            this.bgColor = event.target.value;
        }
    }
});

app.mount("#assignment");
